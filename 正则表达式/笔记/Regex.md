1. 正则是一种编程语言.可以视作一种具有匹配或者查找内容的一种编程语言
2. 编程语言模块化
   输入(指定的正则内容(字符串))->解析处理->输出(匹配的文本内容)
3. 达到某种功能的特殊性编程语言
4. 参考编程语言的类比

|---|---|----|
|---|---|----|
|关键字|class public 等|量词 元组 等|
|流程控制|
|顺序结构||就是组合起来的 正则 |
|选择结构|用分组 或者 "\|" 符号 |
|循环结构|量词匹配 * ? +|
|作用域| 符号 ()|

5. 