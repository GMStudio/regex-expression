Democrat Joe Biden has taken a huge step toward capturing the White House, with wins in Michigan and Wisconsin bringing him close to a majority as the campaign of United States President Donald Trump responded by taking legal action to suspend vote counting.

In a brief address on national television, flanked by US national flags and his vice-presidential pick Kamala Harris, Biden said he wasn't yet declaring victory, but that "when the count is finished, we believe we will be the winners".

By winning the northern battlegrounds of Michigan and Wisconsin, Biden added to his electoral vote count while Trump's total remained fixed on 214. Biden has multiple paths to reach the magic number of 270 needed to win the White House, while Trump's prospects are limited.

US presidential elections are decided not by the popular vote but by securing a majority in the state-by-state Electoral College, which has 538 members.

US media organizations called Michigan for Biden, 77, where he had a lead of nearly 120,000 votes. Earlier, Biden claimed Wisconsin, with a narrower but insurmountable lead.

The two states put the Democrat within striking range of making Trump the first one-term president in 28 years.

However, Trump, 74, claimed victory unilaterally and made clear he would not accept the reported results, issuing unprecedented complaints－unsupported by evidence－of fraud.

"The damage has already been done to the integrity of our system, and to the presidential election itself," he said on social media, alleging that "secretly dumped ballots" had been added in Michigan.

Trump's campaign announced lawsuits in Michigan, Pennsylvania and Georgia and demanded a recount in Wisconsin.

In Michigan, the campaign filed a suit to halt vote counting, saying its "observers" were not allowed to closely watch.

In Detroit, a Democratic stronghold that is majority black, a crowd of mostly white Trump supporters chanted "Stop the count!" and tried to barge into an election office before being blocked by security.

The Trump campaign said it was suing to halt the counting of votes in Pennsylvania, after the president called overnight for Supreme Court intervention to exclude the processing of mail-in ballots after the close of polls.

'Meet halfway'

Meanwhile, China expects the incoming US administration to meet China halfway, manage differences and push for the advancing of China-US ties along the right track, Vice-Foreign Minister Le Yucheng said on Thursday.

Le said that at present, as the counting of the votes is still underway and the result has not yet been finalized, Beijing hopes the election will proceed smoothly and successfully.

China's attitude toward its relations with the US is clear and consistent, Le said, adding that despite differences between the two countries, they also "have extensive common interests and room for cooperation".

"Maintaining and promoting the healthy and stable growth of China-US relations is in line with the fundamental interests of the people of both countries and serves the common expectations of the international community," he said."he said in world said "

It is hoped that the incoming US administration will meet China halfway, adhere to the principle of no conflicts, no confrontations, mutual respect and win-win cooperation, focus on cooperation, manage differences, and push for the advancing of China-US ties, Le said.

The most crucial－and messiest－contest in the US presidential race may yet wind up being in Pennsylvania, where Trump's lead had narrowed to less than 200,000 votes.

"We have to be patient," said Tom Wolf, the Democratic governor of the state, which intends to count mail-in ballots that arrived after Election Day as long they were postmarked by that day. The Trump campaign said it will file a lawsuit to block that action.

"They're going to be counted accurately and they will be counted fully," Wolf told reporters.

The race also tightened in Georgia, a state once seen as solidly Republican, where Trump was up by slightly less than 40,000 votes.

The US Elections Project estimated total turnout at a record 160 million, including more than 101.1 million early voters, 65.2 million of whom cast ballots by mail amid the pandemic.

Agencies, Ai Heping in New York, Zhang Yunbi and Cui Haipei in Beijing contributed to this story.

